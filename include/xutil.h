#ifndef __XUTIL_H
#define __XUTIL_H

#define POW_OF_2(n)     (((n) & ((n) - 1)) == 0) 
#define ABS(x)          ((x) >= 0 ? (x) : -(x))

#define MIN(a, b)       ((a) <= (b) ? (a) : (b))
#define MAX(a, b)       ((a) >= (b) ? (a) : (b))

/* heap */
extern void *xcalloc(unsigned int nl, unsigned int size);
extern void *xmalloc(unsigned int size);
extern void *xrealloc(void *optr, unsigned int size);
extern void xfree(void *ptr);

/* I/O */
extern size_t xread(int fd, void *buf, size_t size);
extern size_t xwrite(int fd, const char *buf, size_t size);

/* string */
extern void xskipwhitespace(const char *str);
extern unsigned long int xstroul(const char *str, char **end, int base);
extern size_t xstrlcpy(char *dst, const char *str, size_t len);
extern char *xstrcpy(char *dst, const char *str);

extern int eq(const char *a, const char *b);
extern int neq(const char *a, const char *b);

extern void print_array(int *a, long int size);

#endif /* __XUTIL_H */
