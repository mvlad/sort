#ifndef __LOG_H
#define __LOG_H

#define str(s)  #s
#define xstr(s) str(s)
#define LOGPREF __FILE__":"xstr(__LINE__)

#define D_PREF  ""
#define D_VERB  ""

extern unsigned int verb;

#ifdef ENABLE_DEBUG
/**
 * @brief normal print 
 */
#define dprintf(format, args...) do {                                   \
        fprintf(stderr, format, ##args);                                \
} while (0)

/**
 * @brief prints FILE:LINE_NO message
 */
#define ddprintf(format, ...) do {                                      \
        fprintf(stderr, LOGPREF" "format, ##__VA_ARGS__);               \
} while (0)
#else
#define dprintf(format, ...)    do { } while (0)
#define ddprintf(format, ...)   do { } while (0)
#endif

#define eprintf(format, args...) do {                                   \
        if (verb)                                                       \
                fprintf(stderr, format, ##args);                        \
} while (0)

#endif /* __LOG_H */
