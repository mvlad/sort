#ifndef __QSORT_H
#define __QSORT_H

extern void
__qsort(int *a, int len);

extern void
__qsort_s(int *a, int n);

#endif /* __QSORT_H */
