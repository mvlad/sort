#ifndef __HEAP_H
#define __HEAP_H


/**
 * @brief basic (max) heap ds
 *
 * @note we assume we have a max heap so no comparison function
 * @note we assume we store only integers so no need 
 * to further abstract it
 */
struct heap {

        unsigned long *item;    /**< array storing the items, specific to ints */
        long int cap;           /**< for resizing the heap, should match len */
        long int len;           /**< how many items are currently in the heap */
};

/**
 * @brief initialize the heap
 * @retval a pointer to a newly allocated struct heap
 *
 * @note caller is responsible for free'ing it
 */
extern struct heap *
heap_init(void);

/**
 * @brief de-allocate/free the heap
 * @param h a pointer to a struct heap
 */
extern void 
heap_free(struct heap *h);

/**
 * @brief insert an item in the heap
 * @param h a pointer to a struct heap
 *
 * @note item is added at the end and
 */
extern void
heap_insert(struct heap *h, unsigned long int item);


/**
 * @brief remove and retrieve the first item from the heap
 * @param h a pointer to a struct heap
 */
extern unsigned long int 
heap_rem(struct heap *h);

/**
 * @brief display all items in the heap
 * @param h a pointer to a struct heap
 */
extern void
heap_print(struct heap *h);

/**
 * @brief given a key, print all children for that key
 * @param h a pointer to a struct heap
 */
extern void
heap_print_childs(struct heap *h, long int key);

/**
 * @brief for each key print its children
 * @param h a pointer to a struct heap
 */
extern void
heap_print_all_child(struct heap *h);

/**
 * @brief retrive the childs for key
 * @param h 
 * @param key
 * @param childs
 *
 * @retval 
 */
extern int
heap_get_childs(struct heap *h, long int key, unsigned long int *childs);

/**
 * @brief given a key, retrieve the parent for that key
 * @param h a pointer to a struct heap
 */
extern unsigned long int
heap_get_parent(struct heap *h, long int key);

/**
 * @brief recursively determine if the heap is a valid max-heap
 * @param h a pointer to a struct heap
 */
extern int
heap_is_valid(struct heap *h, long int key);


/**
 * @brief get the (first) maximum value (item) from the heap
 * @param h a pointer to a struct heap
 *
 * @note being a max-heap the first item is the maximum value
 */
extern unsigned long int
heap_get_max(struct heap *h);

/**
 * @brief get the second maximum item from the heap
 * @param h a pointer to a struct heap
 *
 * @note as the MAXIMUM between the children of the parent node
 */
extern unsigned long int
heap_get_2nd_max(struct heap *h);

/**
 * @brief get the third maximum item from the heap
 * @param h a pointer to a struct heap
 *
 * @note as the MINIUM between the children of the parent node
 */
extern unsigned long int
heap_get_3rd_max(struct heap *h);

/**
 * @brief get the (first) minimum item from the heap
 * @param h a pointer to a struct heap
 */
extern unsigned long int
heap_get_min(struct heap *h);

/**
 * @brief get the second minium item from the heap
 * @param h a pointer to a struct heap
 */
extern unsigned long int
heap_get_2nd_min(struct heap *h);

/**
 * @brief get the third minium item from the heap
 * @param h a pointer to a struct heap
 */
extern unsigned long int
heap_get_3rd_min(struct heap *h);

#endif /* __HEAP_H */
