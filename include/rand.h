#ifndef __RAND_H
#define __RAND_H

extern int 
get_rand(long int len);

extern int *
gen_array(long int len);


/**
 * @brief a fast RNG
 * @param seed a initial seed value
 *
 * @note
 * $I_{j + 1} = a * I_j + c$ Knuth suggests a = 1664525 as a suitable multiplier.
 * H.W. Lewis has conducted extensive tests of this value of a with 
 * c = 1013904223, which is a prime close to \sqrt(5 - 2) * m.
 */
extern unsigned long int 
qrand(unsigned long seed);

/**
 * @brief returns as an integer a random bit, based on the 18 
 * low-significance bits in iseed 
 *
 * @note iseed is modified for the next call
 */
extern int 
irbit(unsigned long *iseed);

#endif /* __RAND_H */
