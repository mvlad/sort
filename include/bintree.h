#ifndef __BIN_TREE_H
#define __BIN_TREE_H


/**
 * @brief 
 */
struct node {
        int item;

        struct node *left;
        struct node *right;

        struct node *parent;
};

/**
 * @brief storage for whole tree
 */
struct bintree {
        struct node *rnode;
        int count;
};

#define NODE_ON_LEFT(node)      ((node)->parent->left == node)
#define NODE_ON_RIGHT(node)     ((node)->parent->right == node)

/**
 * @brief initialize a new binary tree
 */
extern struct bintree *
init_tree(void);

/**
 * @brief free a binary tree
 */
extern void
free_tree(struct bintree *tree);

/**
 * @brief allocate a new node
 */
extern struct node *
new_node(int item);

/**
 * @brief lookup an item
 * @param node
 * @parma 
 */
extern int 
node_lookup(struct node *node, int item);

/**
 * @brief iterative method of inserting a node
 * @param root a pointer to the root of the tree
 * @param item the item to insert
 */
extern void
node_insert_iter(struct bintree *root, int item);

/**
 * @brief recursively insert a node into binary tree
 * @param root a pointer to the root of the tree
 * @param item the item to insert
 */
extern void
node_insert(struct bintree *root, int item);

/**
 * @brief finds and removes node from the tree
 * @param node the node to remove
 */
extern void 
node_remove(struct node *rnode, struct node *node);

/**
 * @brief determine if node has parent
 * @param node the node in question
 * @return 1 in case of success
 * @return 0 if not
 */
extern int
node_has_parent(struct node *node);

/**
 * @brief (recursive) in-order method of printing node contents
 * @param node the node in question
 */
extern void
node_print(struct node *node);

/**
 * @brief (recursive) post-order method of printing node contents
 * @param node the node in question
 */
extern void
node_print_post(struct node *node);

/**
 * @brief (recursive) pre-order method of printing nodes contents
 * @param node the node in question
 */
extern void
node_print_pre(struct node *node);

/**
 * @brief (recursive) the number of nodes along the longest path from * node
 * @param node the node in question
 */
extern int
node_get_depth(struct node *node);

/**
 * @brief retrieve the node with the largest item 
 * @param tree a pointer to the root node of the binary tree
 * @retval a pointer to the node
 */
extern struct node *
node_get_max(struct node *node);

/**
 * @brief retrieve the node with the smalles item value
 * @param tree a pointer to the root node of the binary tree
 * @retval a pointer to the node
 */
extern struct node *
node_get_min(struct node *node);

/**
 * @brief print all items in the binary tree
 * @param tree a pointer to the root node of the binary tree
 */
extern void
tree_print(struct bintree *tree);

/**
 * @brief print all items in the binary tree
 * @param tree a pointer to the root node of the binary tree
 */
extern void
tree_print_path(struct bintree *tree);

/**
 * @brief retrieve the node with the largest item value
 * @param tree a pointer to the root node of the binary tree
 * @retval a pointer to the node
 */
extern struct node *
tree_get_max(struct bintree *tree);

/**
 * @brief retrieve the node with the smalles item value
 * @param tree a pointer to the root node of the binary tree
 * @retval a pointer to the node
 */
extern struct node *
tree_get_min(struct bintree *tree);


/**
 * @brief (recursive) the number of nodes along the longest path from 
 * the root node down to the farthest leaf node.
 * @param tree a pointer to the root of the tree
 */
extern int
tree_get_depth(struct bintree *tree);

/**
 * @brief retrieve the node with the largest item value
 * @param root a pointer to the root node of the binary tree
 * @retval a pointer to the node
 */
extern struct node *
tree_get_2nd_max(struct bintree *tree);

/**
 * @brief retrieve the node with the largest item value
 * @param root a pointer to the root node of the binary tree
 * @retval a pointer to the node
 */
extern struct node *
tree_get_3nd_max(struct bintree *tree);


/**
 * @brief recursively count the items in a binary tree
 * @param a pointer to the root node of the binary tree
 * @return how many nodes are in the tree
 */
extern long int 
tree_count(struct bintree *root);

/**
 * @brief lookup item in a binary tree
 * @param tree a pointer to the binary tree
 * @param item data to look after
 * @return 1 if the item is in the tree
 * @return 0 item is not in the tree
 */
extern int 
tree_lookup_node(struct bintree *tree, int item);

/**
 * @brief check if given tree is a binary search tree
 * @param tree a pointer to the tree
 * @return 1 if the tree is a binary search tree
 * @return 0 if the tree is *not* a binary search tree
 */
extern int
tree_is_bst(struct bintree *tree);

/**
 * @brief determine if two given trees are the same
 * @param t1 a pointer to the first tree
 * @param t2 a pointer to the second tree
 * @return 1 if trees are one and the same
 * @return 0 if the trees are different
 */
extern int 
tree_eq(struct bintree *t1, struct bintree *t2);

/**
 * @brief remove node from the tree
 * @param tree a pointer root node
 * @param node the node to remove
 * @return 1 if success
 * @return 0 on failure
 */
extern int
tree_node_remove(struct bintree *tree, struct node *node);

#endif /* __BIN_TREE_H */
