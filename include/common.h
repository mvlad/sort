#ifndef __COMMON_H
#define __COMMON_H

enum {
        UNUSED,
        PRINT,
        ENTRIES,
        METHOD
};

#define SET_FLAG(flag)          ((1 << flag))
#define FLAG_IS_SET(flag)       ((1 << flag) & rflag)

#endif /* __COMMON_H */
