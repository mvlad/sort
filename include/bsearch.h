#ifndef __BSEARCH_H
#define __BSEARCH_H


/** error for binary search */
#define KEY_NOT_FOUND           (-1)
/** choose a middle for binary search */
#define MID_POINT(min, max)     ((min) + (((max) - (min)) / 2))

/**
 * @brief perform a binary search on array a for key k
 * @param a a pointer a sorted array
 * @param key the key to search for
 * @param n the length of the array
 * @return the entry (id) in the array holding the key k
 * @return -1 in case the key is not found
 */
__inline int
__bsearch(int *a, int key, int n);

/**
 * @brief compute union of two sorted arrays
 * @param a a pointer to first array
 * @param b a pointer to the second array
 * @param m the length of array a
 * @param n the length of array b
 * @param c a pointer to an allocate space where to save the result
 * @retval the number of items saved in c
 *
 * @warning works only on sorted and distinct arrays
 * @noted caller is responsible for allocating and free'ing the result
 */
int 
__union(int *a, int *b, int m, int n, int *c);

/**
 * @brief compute intersection of two sorted arrays
 * @param a a pointer to first array
 * @param b a pointer to the second array
 * @param m the length of array a
 * @param n the length of array b
 * @param c a pointer to an allocate space where to save the result
 * @retval the number of items saved in c
 *
 * @warning works only on sorted and distinct arrays
 * @noted caller is responsible for allocating and free'ing the result
 */
int 
__intersection(int *a, int *b, int m, int n, int *c);

#endif /* __BSEARCH_H */
