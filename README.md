Sorting
=======

A collection of tests for various algorithms and data structures.
Various application scenarious are given for each particular
data structure:

* [BST](#binary-search-tree)
* [Heap](#heap)
* [Sets](#sets)

Building and running
--------------------

### Depencies ###

* `POSIX_C_SOURCE=200809L` (for `getopt(3)` and `clock_gettime(3)`)
* gmake
* a recent compiler

### Building/Running ###

Typing `make` would build all test apps:

- *bst* -- finding maximum in a BST 
- *mheap* -- finding maximum/minium maximum heap app
- *sets* -- performing intersection and reunion on sets

Alternatively type `make help` to see available list of options.

All programs have some default values in case no parameters are supplied at
run-time, so invoke the help associated with each app (`-h`, `--help`) to see what 
type of parameters can you pass to the respective application. Both `bst` and 
`mheap` application can randomly generate and insert items into the associated
data structures. The same can be said with `sets` program.

------------

Binary search tree
------------------

### Problem statement ###

Given a binary search tree with at least `3` integer elements. Devise an algorithm
which prints the first, the second and the third biggest elements stored in this
data structure.

### Background ###

A *binary tree* data structure can be described as follows:

Each node has at most two children -- referred to as the left child and the right
child). 

A *binary search tree* (also called an ordered or sorted binary tree):

A node-based binary tree data structure where each node has a comparable key --
and an associated value. Each node has no more than two child nodes. Each child
must either be a leaf node or the root of another binary search tree. The *left*
sub-tree contains only nodes with keys *less* than the parent node; the *right*
sub-tree contains only nodes with keys *greater* than the parent node.

### Solution ###

* retrieving the maximum node is a matter of iterating on the right-side of the
tree (see `node_get_max()` or `tree_get_max()`)

* for retrieving the second maximum, we use the maximum value and test either
its left side is empty, case in which we take its parent, or if not, iterate
over the right-side of the left node to retrieve the maximum (see
`tree_get_2nd_max()`)

* the third, and last maximum, will add one more condition for the left-node, as
to take into consideration that the parent node of the second maximum
could be the same as the (first) maximum, case in which we use the parents' parent
(see `tree_get_3nd_max()`)

### Example ###

The test application is `bst`

    Test retrieval from a BST
    Usage: ./bst [-v] [-e no_entires] [-p]
            -h                   this help message
            -v                   enable verbose
            -e   no_entries      randomly insert no_entries
            -p                   print tree and its children

### Default values ###

Running the application with default values 

~~~~~~
$ ./bst -p
~~~~~~

The tree for the default values is drawn bellow:

![BST](doc/bst.png)


---------

Heap
----

### Problem statement ###

Given an max-heap data structure implemented as a vector. The data structure
contains `n > 3` elements of distinct integer numbers. Devise an algorithm, which
in a optimal number of steps, prints the 3 biggest and smallest elements from
that data structure.

### Background ###

A *heap data* structure can be described as follows:

If **A** is a parent node of **B** then the key of node **A** is ordered with respect to the
key of node **B** with the same ordering applying across the heap:

- **max-heap**: The keys of parent nodes are always greater than or equal to
those of the children and the highest key is in the root node
- **min-heap**: The keys of parent nodes are less than or equal to those of the
children and the lowest key is in the root node

### Solution ###

#### Computing the maximum ####

* given that this is a max-heap, 
the maximum value is the first entry in the array (see `heap_get_max()`)

* the second maximum is the maximum value of the children's parent node 
(see `heap_get_2nd_max()`)

* the third maximum is the minimum value of the children's parent node
(see `heap_get_3rd_max()`)

#### Computing the minimum ####

As each node contains the maximum element of the subtree below it,
the minimum element can be any leaf in the tree.

* to find the minimum search all the leafs (range between `n/2` and `n - 1`
items) in the heap (see `heap_get_min()`)

* for the second minimum, use `heap_get_min()` to get the first minimum, then
search for the second minimum between `n/2 + 1` and `n - 1` while the item
is bigger than the first minimum (see `heap_get_2nd_min()`)

* for the last and third minimum, we use the range `n/2 + n/4 + 1` and `n - 1`, and
use the second minimum as the value to compare against when computing the minium 
(see `heap_get_3rd_min()`)

### Example ###

The test application is `mheap`

    Test retrieval from a (max)heap
    Usage: ./mheap [-v] [-e no_entries] [-p]
            -h                   this help message
            -v                   enable verbose
            -e   no_entries      randomly insert no_entries
            -p                   print heap

### Default values ###

Running the application with default values 

~~~~~
$ ./mheap -p
~~~~~

The heap with the default values is drawn bellow:

![m-heap](doc/mheap.png)

----------------

Sets
----

### Problem statement ###

Given two vectors with distinct numbers. Devise and algorithm which, in an
optimal number of steps, builds a vector for reunion and intersection of
the two vectors.

Example:

- Input: `A = [4, 7, 10, 3, 1, 9]`, `B = [2, 3, 1, 7, 5, 8, 4]`
- Output: `U = [4, 7, 10, 3, 1, 9, 2, 5, 8]`, `/\ = [4, 7, 3, 1]`

### Solution ###

There are a couple of alternatives:

- determine which array is smaller and sort it, then perform a binary
search for the entries/items of the big array (*small* method in the
test application -- `un_join_sort_small()`)

- sort both arrays and perform union and intersection on the
sorted arrays (*both* method in the test application -- `un_join_sort_both()`)

- sorth both arrays but instead of calling separate functions
to compute the union and intersection, to that directly
(*opt* method in the test application -- `un_join_sort_opt()`)

### Complexity ###


### Running ###

The test application is `sets`

    Test union and intersection on two large arrays
    Usage: ./sets [-p] [-e entries] -m method
            -h   this help message
            -v   enable verbose
            -p   print arrays (generated, sorted) and final results
            -e   randomly generate entries for populating arrays: 'no1,no2'
            -m   method to use: 'small', 'both', 'opt'
                         'small' -- sort only the small array and perform binary search on it
                         'both'  -- sort both arrays
                         'opt'   -- sort both arrays but compute directly the union and intersection

#### Example ####

- the *opt* method:

~~~~~~
$ ./sets -p -e 5,7 -m opt
~~~~~~

- the *default* (*small*) method:

~~~~~~
$ ./sets -p -e 5,7
~~~~~~