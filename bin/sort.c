#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "log.h"
#include "common.h"
#include "xutil.h"
#include "qsort.h"
#include "rand.h"
#include "timer.h"

#define MIL     (1000 * 1000)
#define A_SIZE  (1 * MIL)

unsigned int rflag = 0;
unsigned int verb = 0;
long int nr_entries = 0;

static void 
help(void)
{
        fprintf(stderr, "Test sorting\n");
        fprintf(stderr, "Usage: ");
        fprintf(stderr, "./sort [-v] [-e no_entires] [-p]\n");
        fprintf(stderr, "\t-h                   this help message\n");
        fprintf(stderr, "\t-v                   enable verbose\n");
        fprintf(stderr, "\t-e   no_entries      no of entries to generate\n");
        exit(EXIT_FAILURE);
}


static void
parse_argv(int argc, char *argv[])
{
        int c;

        while ((c = getopt(argc, argv, "hve:")) != -1) {
                switch (c) {
                case 'h':
                        help();
                        break;
                case 'v':
                        verb = 1;
                        break;
                case 'e': 
                        nr_entries = atol(optarg);
                        if (nr_entries <= 3) {
                                eprintf("Invalid # of entries!\n");
                                help();
                        }
                        rflag |= SET_FLAG(ENTRIES);
                        eprintf("entires=%ld!\n", nr_entries);
                        break;
                default:
                        help();
                        break;
                }
        }
}


int main(int argc, char *argv[])
{
        int *va;
        long int entries;

        INIT_TIMER(t1);
        INIT_TIMER(t2);

        parse_argv(argc, argv);

        if (FLAG_IS_SET(ENTRIES))
                entries = nr_entries;
        else
                entries = A_SIZE;

        dprintf("generating array a (%d)\n", entries);
        va = gen_array(entries);

        /* sort them */
        eprintf("sorting using __qsort_s()\n");
        START_TIMER(t1);
        __qsort_s(va, entries);
        STOP_TIMER(t1);

        xfree(va);

        dprintf("generating array a (%d)\n", entries);
        va = gen_array(entries);

        eprintf("sorting using __qsort()\n");
        START_TIMER(t2);
        __qsort(va, entries);
        STOP_TIMER(t2);

        SHOW_WALLCLOCK("__qsort_s() took: ", t1);
        SHOW_WALLCLOCK("__qsort() took: ", t2);

        xfree(va);
        return 0;
}
