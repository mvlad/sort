#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "log.h"
#include "common.h"
#include "xutil.h"
#include "qsort.h"
#include "bsearch.h"
#include "rand.h"
#include "timer.h"

unsigned int verb = 0;
unsigned int rflag = 0;

/** default no of entries */
#define MIL     1
#define A_SIZE  (17 * MIL)
#define B_SIZE  (9 * MIL)

/**
 * @brief storage to pass (pre)-generated arrays and their sizes
 */ 
struct arrays {
        int *small;
        int *big;

        long int len_small;
        long int len_big;
};

/**
 * @brief sort only the smart array and perform binary search
 * with items from the big array
 */
static
void un_join_sort_small(struct arrays *a);

/**
 * @brief sort both arrays 
 */
static
void un_join_sort_both(struct arrays *a);

/**
 * @brief sort both arrays but compute the union and
 * intersion on the fly
 */
static
void un_join_sort_opt(struct arrays *a);

typedef enum {
        METHOD_SMALL, METHOD_BOTH, METHOD_OPT
} method_t;

/**
 * @brief aggregate all methods in one place
 */
struct meth {
        method_t meth;
        char *name;
        void (*method)(struct arrays *a);
} methods[] = {
        { METHOD_SMALL, "small",        un_join_sort_small      },
        { METHOD_BOTH,  "both",         un_join_sort_both       },
        { METHOD_OPT,   "opt",          un_join_sort_opt        },
        { -1,            NULL,           NULL                   }
};

#define ARRAY_SIZE(array)       (sizeof(array)/sizeof(array[0]))

/** user specified arrays */
long int entry_nr_a = 0;
long int entry_nr_b = 0;

/** user specified method */
int meth;


static void 
help(void)
{
        fprintf(stderr, "Test union and intersection on two large arrays\n");
        fprintf(stderr, "Usage: ");
        fprintf(stderr, "./sets [-p] [-e entries] -m method\n");
        fprintf(stderr, "\t-h   this help message\n");
        fprintf(stderr, "\t-v   enable verbose\n");
        fprintf(stderr, "\t-p   print arrays (generated, sorted) and final results\n");
        fprintf(stderr, "\t-e   randomly generate entries for populating arrays: 'no1,no2'\n");
        fprintf(stderr, "\t-m   method to use: 'small', 'both', 'opt'\n");
        fprintf(stderr, "\t             'small' -- sort only the small array and perform binary search on it\n");
        fprintf(stderr, "\t             'both'  -- sort both arrays\n");
        fprintf(stderr, "\t             'opt'   -- sort both arrays but compute directly the union and intersection\n");
        exit(EXIT_FAILURE);
}

/**
 * @brief determine if given method is valid
 * @param meth_name a string given at runtime
 * @retval 0 if meth_name was not found
 * @retval METHOD_SMALL, METHOD_BOTH or METHOD_OPT in case
 * we received a valid method
 */
static int
get_method_type(char *meth_name)
{
        unsigned int i;

        assert(meth_name != NULL);

        for (i = 0; i < ARRAY_SIZE(methods) - 1; i++) {
                if (!strcmp(methods[i].name, meth_name)) {
                        return methods[i].meth;
                }
        }
        return -1;
}

/**
 * @brief retrive index from methods arrays
 * @param meth either METHOD_SMALL, METHOD_BOTH or METHOD_OPT
 * @retval -1 if not found
 */
static int
get_method_index(method_t meth)
{
        unsigned int i;

        for (i = 0; i < ARRAY_SIZE(methods) -1; i++) {
                if (methods[i].meth == meth)
                        return i;
        }
        return -1;
}



/**
 * @brief parse number of entries for randomly generating arrays
 * @param optarg the string given, under the form of 'NUMBER1,NUMBER2'
 * @param e1 a pointer to a long int
 * @param e2 a pointer to a long int
 *
 * @note the method parses the string to long ints and stores
 * them into e1 and e2
 */
static void
parse_entries(char *optarg, long int *e1, long int *e2)
{
        char *p, *ptr;

        int i = 0;
        long int ents[2];

        ptr = strndup(optarg, strlen(optarg));

        p = strtok(ptr, ",");
        while (p && *p) {

                /* sanity, can't allow more than 2 entries */
                if (i > 1) {
                        xfree(ptr);
                        return;
                }

                char *e = p; 
                long int nr = 0;

                /* convert to a long */
                for (; *e != '\0'; e++) {
                        nr = (nr * 10) + *e - '0';                
                }

                /* save it */
                ents[i++] = nr;

                /* go on the next one */
                p = strtok(NULL, ","); 
        }

        xfree(ptr);

        *e1 = ents[0];
        *e2 = ents[1];
}

static void
parse_argv(int argc, char *argv[])
{
        int c;

        while ((c = getopt(argc, argv, "vhe:pm:")) != -1) {
                switch (c) {
                case 'h':
                        help();
                        break;
                case 'v':
                        verb = 1;
                        break;
                case 'p':
                        rflag |= SET_FLAG(PRINT);
                        verb = 1;
                        break;
                case 'e':
                        parse_entries(optarg, &entry_nr_a, &entry_nr_b);

                        if (!entry_nr_a || !entry_nr_b) {
                                eprintf("Invalid no of entries specified\n");
                                help();
                        }

                        rflag |= SET_FLAG(ENTRIES);
                        eprintf("No entries (%ld) (%ld)!\n", entry_nr_a, entry_nr_b);
                        break;
                case 'm': {
                        int method;
                        if ((method = get_method_type(optarg)) == -1) {
                                eprintf("Invalid method ('%s') specified\n", optarg);
                                help();
                        }

                        rflag |= SET_FLAG(METHOD);
                        meth = method;
                }
                        break;
                default:
                        help();
                        break;
                }
        }
}

static void
print_results(int *un, long int size_un, 
              int *comm, long int size_comm)
{
        if (size_un) {
                eprintf("Printing union\n");
                print_array(un, size_un);
        }

        if (size_comm) {
                eprintf("Printing intersection\n");
                print_array(comm, size_comm);
        }
}

static void
un_join_sort_small(struct arrays *a)
{
        int *un, *comm;
        long int i; 
        
        long int len_un, len_comm; 

        /* how much to alloc */
        long int max_union;
        long int max_common;

#ifdef ENABLE_TIMER
        INIT_TIMER(time_sort);
        INIT_TIMER(time_comp);
#endif

        eprintf("Using 'small' method\n");

        /* sort them */
#ifdef ENABLE_TIMER
        START_TIMER(time_sort);
#endif
        __qsort(a->small, a->len_small);
#ifdef ENABLE_TIMER
        STOP_TIMER(time_sort);
#endif

        if (FLAG_IS_SET(PRINT)) {
                eprintf("Printing array sorted\n");
                print_array(a->small, a->len_small);
        }

        max_common      = a->len_big;
        max_union       = a->len_small + a->len_big;

        eprintf("Max common %ld, max union %ld\n", 
                        max_common, max_union);

        un = xcalloc(max_union, sizeof(int));
        comm = xcalloc(max_common, sizeof(int));

        len_un = len_comm = 0;

#ifdef ENABLE_TIMER
        START_TIMER(time_comp);
#endif
        for (i = 0; i < a->len_big; i++) {

                int key_id = __bsearch(a->small, a->big[i], a->len_small);

                /* item in both of them */
                if (key_id != KEY_NOT_FOUND) {
                        comm[len_comm++] = a->small[key_id];
                } else {
                        /* union items */
                        un[len_un++] = a->big[i];
                }
        }

        /* append distinct items from the small one */
        for (i = 0; i < a->len_small; i++) {
                un[len_un++] = a->small[i];
        }

#ifdef ENABLE_TIMER
        STOP_TIMER(time_comp);
#endif

        eprintf("%ld union items, %ld common items\n", 
                        len_un, len_comm);

#ifdef ENABLE_TIMER
        SHOW_WALLCLOCK("Sorting took ", time_sort);
        SHOW_WALLCLOCK("Union + Common took ", time_comp);

        ADD_TM_TO_AM(time_sort, time_comp);

        SHOW_WALLCLOCK("Total time took ", time_sort);
#endif

        if (FLAG_IS_SET(PRINT)) {
                print_results(un, len_un, comm, len_comm);
        }
        xfree(un);
        xfree(comm);
}

static void
un_join_sort_both(struct arrays *a)
{
        int *un, *comm;
        long int len_un, len_comm;

        /* how much to alloc */
        long int max_union;
        long int max_common;


#ifdef ENABLE_TIMER
        INIT_TIMER(time_sort);
        INIT_TIMER(time_union);
        INIT_TIMER(time_comm);
#endif

        eprintf("Using 'both' method\n");

        /* sort them */
#ifdef ENABLE_TIMER
        START_TIMER(time_sort);
#endif
        __qsort(a->big, a->len_big);
        __qsort(a->small, a->len_small);

#ifdef ENABLE_TIMER
        STOP_TIMER(time_sort);
#endif

        if (FLAG_IS_SET(PRINT)) {
                eprintf("Printing small array sorted\n");
                print_array(a->small, a->len_small);
                eprintf("Printing big array sorted\n");
                print_array(a->big, a->len_big);
        }

        max_common = MAX(a->len_small, a->len_big);
        max_union  = a->len_small + a->len_big;

        un = xcalloc(max_union, sizeof(int));


#ifdef ENABLE_TIMER
        START_TIMER(time_union);
#endif
        len_un = __union(a->big, a->small, a->len_big, a->len_small, un);

#ifdef ENABLE_TIMER
        STOP_TIMER(time_union);
#endif


        comm = xcalloc(max_common, sizeof(int));

#ifdef ENABLE_TIMER
        START_TIMER(time_comm);
#endif
        len_comm = __intersection(a->big, a->small, a->len_big, a->len_small, comm);

#ifdef ENABLE_TIMER
        STOP_TIMER(time_comm);
#endif

        eprintf("Common items %ld, union items %ld\n", len_comm, len_un);

#ifdef ENABLE_TIMER
        SHOW_WALLCLOCK("Sorting took ", time_sort);
        SHOW_WALLCLOCK("Union took ", time_union);
        SHOW_WALLCLOCK("Common took ", time_comm);

        ADD_TM_TO_AM(time_sort, time_union);
        ADD_TM_TO_AM(time_sort, time_comm);

        SHOW_WALLCLOCK("Total time took ", time_sort);
#endif
        if (FLAG_IS_SET(PRINT)) {
                print_results(un, len_un, comm, len_comm);
        }

        xfree(un);
        xfree(comm);
}

static void
un_join_sort_opt(struct arrays *a)
{
        int i, j, m, n;

        long int len_un, len_comm;

        /* union and common */
        int *un, *comm;

        long int max_union; 
        long int max_common;

        int *va, *vb;

        va = a->big;
        vb = a->small;

        m = a->len_big;
        n = a->len_small;

#ifdef ENABLE_TIMER
        INIT_TIMER(time_sort);
        INIT_TIMER(time_comb);
#endif
        
        eprintf("Using 'both' method (on-the-fly)\n");


#ifdef ENABLE_TIMER
        START_TIMER(time_sort);
#endif
        __qsort(va, m);
        __qsort(vb, n);

#ifdef ENABLE_TIMER
        STOP_TIMER(time_sort);
#endif

        if (FLAG_IS_SET(PRINT)) {
                eprintf("Printing small array sorted\n");
                print_array(a->small, a->len_small);
                eprintf("Printing big array sorted\n");
                print_array(a->big, a->len_big);
        }

        max_common = MAX(m, n);
        max_union = m + n;

        un = xcalloc(max_union, sizeof(int));
        comm = xcalloc(max_common, sizeof(int));

        eprintf("Computing union and intersection\n");


        len_un = len_comm = 0;

#ifdef ENABLE_TIMER
        START_TIMER(time_comb);
#endif
        for (i = 0, j = 0; i < m && j < n;) {
                if (va[i] < vb[j]) {
                        un[len_un++] = va[i++];
                } else if (va[i] > vb[j]) {
                        un[len_un++] = vb[j++];
                } else {
                        /* doesn't matter just add */
                        un[len_un++] = vb[j];

                        /* store also intersection */
                        comm[len_comm++] = vb[j];

                        /* advance */
                        i++; j++;
                }

        }

        while (i < m) {
                un[len_un++] = va[i++];
        }
        while (j < n) {
                un[len_un++] = vb[j++];
        }

#ifdef ENABLE_TIMER
        STOP_TIMER(time_comb);
#endif

#ifdef ENABLE_TIMER
        SHOW_WALLCLOCK("Sorting took ", time_sort);
        SHOW_WALLCLOCK("Union + Comm took ", time_comb);

        ADD_TM_TO_AM(time_sort, time_comb);

        SHOW_WALLCLOCK("Total time took ", time_sort);
#endif

        if (FLAG_IS_SET(PRINT)) {
                print_results(un, len_un, comm, len_comm);
        }

        xfree(comm);
        xfree(un);
}

/**
 * @brief generate random arrays and stores them into a
 * @param a pointer to struct arrays
 * @param entry_a maximum entries for the first array
 * @param entry_b maximum entries for the second one
 *
 * @note after usage caller should use free_arrays() to 
 * de-allocate the generated arrays
 */
static void
get_array_sizes(struct arrays *a, long int entry_a, long int entry_b)
{
        int min, max;
        int *va, *vb;

        eprintf("Generating arrays (%ld), (%ld)...\n", 
                        entry_a, entry_b);

        /* generate */
        va = gen_array(entry_a);
        vb = gen_array(entry_b);

        if (FLAG_IS_SET(PRINT)) {
                eprintf("Printing first array\n");
                print_array(va, entry_a);
                eprintf("Printing second array\n");
                print_array(vb, entry_b);
        }

        /* figure out which arrays is smaller/bigger */
        max = MAX(entry_a, entry_b);
        min = MIN(entry_a, entry_b);

        if ((max - min)) {
                if (min == entry_a) {
                        a->small = va;
                        a->big = vb;

                        a->len_small = entry_a;
                        a->len_big = entry_b;
                } else {
                        a->small = vb;
                        a->big = va;

                        a->len_small = entry_b;
                        a->len_big = entry_a;
                } 
        } else {
                a->small = vb;
                a->big   = va;

                a->len_small = a->len_big = entry_a;
        }
}

static void
free_arrays(struct arrays *a)
{
        xfree(a->small);
        xfree(a->big);
}

int main(int argc, char *argv[])
{
        struct arrays a;
        int meth_index;

        parse_argv(argc, argv);

        memset(&a, 0, sizeof(struct arrays));
        if (FLAG_IS_SET(ENTRIES)) {
                get_array_sizes(&a, entry_nr_a, entry_nr_b);
        } else {
                get_array_sizes(&a, A_SIZE, B_SIZE);
        }

        meth_index = get_method_index(meth);
        if (meth_index == -1) {
                eprintf("Got invalid entry (%d)\n", meth_index);
                goto err_exit;

        }

        if (FLAG_IS_SET(METHOD)) {
                eprintf("Method given '%s'...\n", methods[meth_index].name);
                methods[meth_index].method(&a);
        } else {
                eprintf("No method was specified, using default\n");
                /* default one */
                methods[0].method(&a);
        }

        free_arrays(&a);
        return 0;

err_exit:
        free_arrays(&a);
        return -1;
}
