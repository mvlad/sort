#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>

#include "heap.h"
#include "log.h"
#include "common.h"
#include "rand.h"
#include "xutil.h"
#include "timer.h"

#if 0
int items[] = {
        17, 1, 7, 2, 25, 3, 100, 36, 19
};
#else
int items[] = {
        19, 1, 18, 2, 16, 3, 11, 4, 23, 5, 6, 7, 8, 9, 10
};
#endif

#define ITEMS_SIZE(items)       (sizeof(items)/sizeof(items[0]))

unsigned int rflag = 0;
unsigned int verb  = 0;

long int nr_entries = 0;

/**
 * @brief
 *
 */
static struct heap *
heap_populate(long int nr_entries)
{
        struct heap *h = heap_init();
        struct timespec ts;
        unsigned long int i;

        if (FLAG_IS_SET(ENTRIES)) {

                for (i = nr_entries; i > 0; i--) {
                        get_current_ts(&ts);

                        unsigned long p = qrand(ts.tv_nsec);
                        irbit(&p);
                        heap_insert(h, p >> 16);
                }
        } else {
                for (i = 0; i < ITEMS_SIZE(items); i++) {
                        heap_insert(h, items[i]);
                }
        }

        return h;
}

static void 
help(void)
{
        fprintf(stderr, "Test retrieval from a (max)heap\n");
        fprintf(stderr, "Usage: ");
        fprintf(stderr, "./mheap [-v] [-e no_entries] [-p]\n");
        fprintf(stderr, "\t-h                   this help message\n");
        fprintf(stderr, "\t-v                   enable verbose\n");
        fprintf(stderr, "\t-e   no_entries      randomly insert no_entries\n");
        fprintf(stderr, "\t-p                   print heap\n");
        exit(EXIT_FAILURE);
}

static void
parse_argv(int argc, char *argv[])
{
        int c;

        while ((c = getopt(argc, argv, "he:vtp")) != -1) {
                switch (c) {
                case 'h':
                        help();
                        break;
                case 'v':
                        verb = 1;
                        break;
                case 'e': 
                        nr_entries = atol(optarg);
                        if (nr_entries <= 3) {
                                eprintf("Invalid # of entries!\n");
                                help();
                        }
                        rflag |= SET_FLAG(ENTRIES);
                        eprintf("entries=%ld!\n", nr_entries);
                        break;
                case 'p':
                        rflag |= SET_FLAG(PRINT);
                        verb = 1;
                        eprintf("Printing is set!\n");
                        break;
                default:
                        help();
                        break;
                }
        }
}

int main(int argc, char *argv[])
{
        struct heap *h;

        unsigned long int max, smax, lmax;
        unsigned long int min, smin, lmin;

#ifdef ENABLE_TIMER
        INIT_TIMER(t_min);
        INIT_TIMER(t_smin);
        INIT_TIMER(t_lmin);
#endif

        parse_argv(argc, argv);

        h = heap_populate(nr_entries);

        /* sanity checks */
        assert(heap_is_valid(h, 0));

        if (FLAG_IS_SET(PRINT)) {
                heap_print(h);
                heap_print_all_child(h);
        }

        max = heap_get_max(h);
        eprintf("Maximum %lu\n", max);

        smax = heap_get_2nd_max(h);
        eprintf("Second maximum %lu\n", smax);

        assert(max != smax);

        lmax = heap_get_3rd_max(h);
        eprintf("Third maximum %lu\n", lmax);

        assert(max != lmax);
        assert(smax != lmax);

        assert(heap_is_valid(h, 0));

#ifdef ENABLE_TIMER
        START_TIMER(t_min);
#endif
        min = heap_get_min(h);
#ifdef ENABLE_TIMER
        STOP_TIMER(t_min);
#endif
        eprintf("Minimum %lu\n", min);

#ifdef ENABLE_TIMER
        START_TIMER(t_smin);
#endif
        smin = heap_get_2nd_min(h);
#ifdef ENABLE_TIMER
        STOP_TIMER(t_smin);
#endif

        eprintf("Second minimum %lu\n", smin);

        assert(min != smin);
        assert(min < smin);

#ifdef ENABLE_TIMER
        START_TIMER(t_min);
#endif
        lmin = heap_get_3rd_min(h);
#ifdef ENABLE_TIMER
        STOP_TIMER(t_min);
#endif
        eprintf("Third minimum %lu\n", lmin);

        assert(lmin != min);
        assert(min < lmin);

        assert(lmin != smin);
        assert(smin < lmin);

#ifdef ENABLE_TIMER
        SHOW_WALLCLOCK("Minium took ", t_min);
        SHOW_WALLCLOCK("SMinium took ", t_smin);
        SHOW_WALLCLOCK("TMinium took ", t_lmin);
#endif

        heap_free(h);
        return 0;
}
