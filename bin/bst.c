#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>

#include "common.h"
#include "timer.h"
#include "bintree.h"
#include "log.h"
#include "rand.h"

unsigned int rflag = 0;
unsigned int verb  = 0;

long int nr_entries = 0;

/** have some default entries in case we don't receive anything */
int tree_a[] = { 
        1080, 9015, 2180, 
        3455, -5325, -7600, 0
};

int tree_b[] = { 
        7, 3, 9, 1, 5, 17, 
        14, 6, 12, 15, 4, 0
};

int *entries[] = { 
        tree_a, tree_b, NULL 
};


static struct bintree *
tree_init(long int nr_entries)
{
        unsigned long int p;
        struct timespec ts;

#ifdef ENABLE_TIMER
        INIT_TIMER(insertion);
#endif

        struct bintree *root = init_tree();

        if (nr_entries) {
                get_current_ts(&ts);
#ifdef ENABLE_TIMER
                START_TIMER(insertion);
#endif
                for (p = nr_entries; p > 1; p--) {
                        get_current_ts(&ts);
                        unsigned long i = qrand(ts.tv_nsec);
                        irbit(&i);
                        /* the iterative method is slightly faster */
                        node_insert_iter(root, (int) i);
                }

#ifdef ENABLE_TIMER
                STOP_TIMER(insertion);
                SHOW_WALLCLOCK("Time took to insert ", insertion);
#endif
        } else {
                /* just add the default values */
                int **entry = entries;
                for (; *entry != NULL; entry++) {
                        int i;

                        int *tree = *entry;
                        for (i = 0; tree[i] != 0; i++) {
                                node_insert(root, tree[i]);
                        }
                }

        }

        return root;
}


static void 
help(void)
{
        fprintf(stderr, "Test retrieval from a BST\n");
        fprintf(stderr, "Usage: ");
        fprintf(stderr, "./bst [-v] [-e no_entires] [-p]\n");
        fprintf(stderr, "\t-h                   this help message\n");
        fprintf(stderr, "\t-v                   enable verbose\n");
        fprintf(stderr, "\t-e   no_entries      randomly insert no_entries\n");
        fprintf(stderr, "\t-p                   print tree and its children\n");
        exit(EXIT_FAILURE);
}

static void
parse_argv(int argc, char *argv[])
{
        int c;

        while ((c = getopt(argc, argv, "he:vp")) != -1) {
                switch (c) {
                case 'h':
                        help();
                        break;
                case 'v':
                        verb = 1;
                        break;
                case 'e': 
                        nr_entries = atol(optarg);
                        if (nr_entries <= 3) {
                                eprintf("Invalid no of entries!\n");
                                help();
                        }
                        rflag |= SET_FLAG(ENTRIES);
                        eprintf("entries=%ld!\n", nr_entries);
                        break;
                case 'p':
                        rflag |= SET_FLAG(PRINT);
                        verb = 1;
                        eprintf("printing is set!\n");
                        break;
                default:
                        help();
                        break;
                }
        }
}

int main(int argc, char *argv[])
{
        struct bintree *tree = NULL;
        struct node *max, *sec_max, *third_max;

        parse_argv(argc, argv);

        /* init tree */
        dprintf("Generating tree... ");
        tree = tree_init(nr_entries);

        if (FLAG_IS_SET(PRINT)) {
                eprintf("printing nodes\n");
                node_print_pre(tree->rnode);

                eprintf("printing node paths from root %d\n", tree->rnode->item);
                tree_print_path(tree);
        }

        /* first sanity test */
        assert(tree_is_bst(tree) != 0);

        /* get biggest value in the tree */
        max = tree_get_max(tree);

        eprintf("Maximum node is %d\n", max->item);
        dprintf("Tree depth %d\n", tree_get_depth(tree));

        /* second sanity test, lookup biggest node */
        assert(node_lookup(tree->rnode, max->item));

        /* second sanity test */
        if (FLAG_IS_SET(ENTRIES)) {
                long int nr = tree_count(tree);
                assert(nr == (nr_entries - 1));
        }

        /* get the 2-nd maximum */
        sec_max = tree_get_2nd_max(tree);
        eprintf("Second maximum node is %d\n", sec_max->item);

        assert(sec_max->item < max->item);

        /* and the third maximum */
        third_max = tree_get_3nd_max(tree);
        eprintf("Third maximum node is %d\n", third_max->item);

        assert(third_max->item <= max->item);
        assert(third_max->item <= sec_max->item);

        dprintf("Removing node %d\n", third_max->item);
        int r = tree_node_remove(tree, third_max);
        if (r) {
                dprintf("Node has been removed!\n");
        }

        assert(tree_is_bst(tree) != 0);

        free_tree(tree);
        return 0;
}
