#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>
#include <errno.h>

#include "log.h"
#include "xutil.h"

void 
*xmalloc(unsigned int size)
{
        void *ptr = malloc(size);

        if (size && !ptr) {
                fprintf(stderr, "memory allocation failed\n");
                exit(EXIT_FAILURE);
        }
        memset(ptr, 0, size);
        return ptr;
}

void 
*xcalloc(unsigned int nl, unsigned int size)
{
        void *ptr = calloc(nl, size);

        if (nl && size && !ptr) {
                fprintf(stderr, "memory callocation failed\n");
                exit(EXIT_FAILURE);
        }
        return ptr;
}

void
*xrealloc(void *ptr, unsigned int size)
{
        void *nptr; 

        nptr = realloc(ptr, size);

        if (!nptr) {
                fprintf(stderr, "memory re-allocation failed\n");
                exit(EXIT_FAILURE);
        }
        return nptr;
}

void 
xfree(void *ptr)
{
        if (ptr) {
                free(ptr);
        }
        ptr = NULL;
}

size_t 
xread(int fd, void *buf, size_t size)
{
        ssize_t total = 0;

        while (size) {
                ssize_t ret;
                ret = read(fd, buf, size);

                if (ret < 0 && errno == EINTR)
                        continue;
                /* no more to read */
                if (ret <= 0)
                        break;
                buf = (char *) buf + ret;
                size -= ret;
                total += ret;
        }

        return total;
}

size_t 
xwrite(int fd, const char *buf, size_t size)
{
        ssize_t total = 0;

        while (size) {
                ssize_t ret;
                ret = write(fd, buf, size);

                if (ret < 0 && errno == EINTR)
                        continue;
                /* no more to write */
                if (ret <= 0)
                        break;

                buf = (const char *) buf + ret;
                size -= ret;
                total += ret;
        }

        return total;
}

char 
*xstrcpy(char *dst, const char *src)
{
        char *ret;
        ret = dst;

        while ((*dst++ = *src++) != '\0')
                /* do nothing */;
        return ret;
}

size_t
xstrlcpy(char *dst, const char *src, size_t size)
{
        char *dst_in;

        dst_in = dst;
        if (size > 0) {
                while (--size > 0 && *src != '\0')
                        *dst++ = *src++;
                *dst = '\0';
        }
        return dst - dst_in;
}

void
print_array(int *a, long int s)
{
        long int i;

        if (s) {
                for (i = 0; i < s; i++)
                        eprintf("%d ", a[i]);
                eprintf("\n");
        }
}
