#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "heap.h"
#include "xutil.h"
#include "log.h"

struct heap *
heap_init(void)
{
        struct heap *heap = xcalloc(1, sizeof(*heap));
        return heap;
}

void 
heap_free(struct heap *heap)
{
        xfree(heap->item);
        xfree(heap);
}


static __inline 
void swap(struct heap *h, long int i, long int j)
{
        unsigned long int tmp = h->item[i];

        h->item[i] = h->item[j];
        h->item[j] = tmp;
}

/**
 * @brief adjust the contents of the heap when adding a new element
 * @param h a pointer to the heap 
 * @param key the length of the heap, which is the new item added
 *
 * @note 
 *
 * - chooses parent and compare with node added
 * - If condition does not hold, swap the element with its parent
 * 
 */
static void 
bubble_up(struct heap *h, long int key)
{
        /* parent */
        long int p; 

        while (key) {
                /* choose the parent */
                p = (key - 1) / 2;

                /* determine if we need to swap */
                if (h->item[key] > h->item[p]) {

                        /* swap the elements */
                        swap(h, p, key);

                        /* pick a new maximum */
                        key = p;
                } else {
                        break;
                }
        }
}

/**
 * @brief keep heap properties after removing an item
 *
 */
static void 
bubble_down(struct heap *h, long int i)
{
        /* left, right child */
        long int l, r;
        long int min;

        while (1) {
                l = 2 * i + 1;

                if (l >= h->len) {
                        break;
                }

                /* or 2 * i + 2 */
                r = l + 1;

                if (r >= h->len) {
                        min = l;
                } else {
                        if (h->item[l] > h->item[r]) {
                                min = l;
                        } else {
                                min = r;
                        }
                }

                if (h->item[min] > h->item[i]) {
                        swap(h, i, min);
                        i = min;
                } else {
                        break;
                }
        }
}

void 
heap_insert(struct heap *h, unsigned long int item)
{

        if (h->len == h->cap) {
                unsigned long *nitem = xrealloc(h->item, 
                                (h->cap + 1) * sizeof(unsigned long));
                h->item = nitem;
                h->cap++;
        }

        h->item[h->len] = item;
        bubble_up(h, h->len);

        h->len++;
}

unsigned long int
heap_rem(struct heap *h)
{
        unsigned long int ret = h->item[0];

        /* swap root with last element added */
        swap(h, 0, --h->len);

        if (h->len) {
                bubble_down(h, 0);

                /* resize */
                if (h->len == h->cap - 4096) {
                        unsigned long *nitem = xrealloc(h->item, 
                                        h->len * sizeof(unsigned long));
                        h->item = nitem;
                        h->cap = h->len;
                }
        }

        return ret;
}

void
heap_print(struct heap *h)
{
        long int i;
        eprintf("Items in heap (%ld)\n", h->len);
        for (i = 0; i < h->len; i++) {
                eprintf("(%ld) => %lu \n", i, h->item[i]);
        }
        eprintf("\n");
}

void
heap_print_childs(struct heap *h, long int key)
{
        unsigned long int child1, child2;

        if (key > ((h->len / 2) - 1)) {
                return;
        }

        /* starts from 0 */
        child1 = h->item[2 * key + 1];
        child2 = h->item[2 * key + 2];

        eprintf("Children of %lu are\n", h->item[key]);
        eprintf("left=%lu, right=%lu\n", child1, child2);
}


void
heap_print_all_child(struct heap *h)
{
        unsigned long int child1, child2;
        long int i;

        for (i = 0; i < (h->len / 2); i++) {

                /* starts from 0 */
                child1 = h->item[2 * i + 1];
                child2 = h->item[2 * i + 2];

                eprintf("Children of %lu are\n", h->item[i]);

                if (child1)
                        eprintf("left=%lu", child1);
                if (child2)
                        eprintf(", right=%lu", child2);

                eprintf("\n");
        }
}

int
heap_get_childs(struct heap *h, long int key, unsigned long int *childs)
{
        if (key >= (h->len / 2) - 1)
                return 0;

        /* starts from 0 */
        childs[0] = h->item[2 * key + 1];
        childs[1] = h->item[2 * key + 2];

        return 1;
}

unsigned long int
heap_get_parent(struct heap *h, long int key)
{
        unsigned long int p;

        if (key > h->len -1) {
                return -1;
        }

        p = h->item[(key - 1) / 2];
        return p;
}

int
heap_is_valid(struct heap *h, long int key)
{
        long int l, r;

        l = 2 * key + 1;
        r = 2 * key + 2;

        if (l < h->len) {
                if (h->item[l] > h->item[key]) {
                        return 0;
                }

                if (!heap_is_valid(h, l)) {
                        return 0;
                }
        }

        if (r < h->len) {
                if (h->item[r] > h->item[key]) {
                        return 0;
                }
                if (!heap_is_valid(h, r))
                        return 0;
        }

        return 1;
}

unsigned long int
heap_get_max(struct heap *h)
{
        return h->item[0];
}

unsigned long int
heap_get_2nd_max(struct heap *h)
{
        return MAX(h->item[1], h->item[2]);        
}

unsigned long int
heap_get_3rd_max(struct heap *h)
{
        return MIN(h->item[1], h->item[2]);
}

unsigned long int
heap_get_min(struct heap *h)
{
        long int i;

        /* 
         * leafs in the heap start when the last parent ends:
         *
         * 2 * key + 1 <- child
         * 2 * key + 2 <- child
         *
         *
         * [0], [1], [2] ..... [x] .... [n - 1]
         * \................../ |\............/
         *         parent       |     leafs
         *          nodes       |
         *                      x = n / 2
         * 
         */
        long int mid = h->len / 2;

        unsigned long int min = h->item[0];

        for (i = mid; i < h->len; i++) {
                if (h->item[i] < min)
                        min = h->item[i];
        }

        return min;
}

unsigned long int
heap_get_2nd_min(struct heap *h)
{
        long int i;

        /* exclude the first leaf from all the leafs */
        long int mid = (h->len / 2) + 1;

        /* use the first minimum as lower value */
        unsigned long min = heap_get_min(h);

        unsigned long int nd_max = h->item[0];

        for (i = mid; i < h->len; i++) {
                /*
                 * it might happen that the minimum previously
                 * found is farther way so the second minimum must be
                 * bigger than that
                 */
                if (h->item[i] < nd_max && h->item[i] > min)
                        nd_max = h->item[i];
        }

        return nd_max;
}

unsigned long int
heap_get_3rd_min(struct heap *h)
{
        long int i;

        /* use the second minimum as a lower value */
        unsigned long min = heap_get_2nd_min(h);

        long int mid = (h->len / 2) + (h->len / 4) + 1;

        unsigned long rd_min = h->item[0];

        for (i = mid; i < h->len; i++) {

                /* apply the same tactic as in second minium */
                if (h->item[i] < rd_min && h->item[i] > min)
                        rd_min = h->item[i];
        }

        return rd_min;
}
