#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include "xutil.h"
#include "bintree.h"
#include "log.h"

#define MAX_PATHS (1024 * 1024)

struct bintree *
init_tree(void)
{
        struct bintree *tree = xcalloc(1, sizeof(*tree));

        tree->count = 0;
        tree->rnode = NULL;

        return tree;
}

/**
 * @brief recursively free/remove nodes
 * @param node a pointer to a node
 */
static void
delete_node(struct node *node)
{
        if (node->left)
                delete_node(node->left);
        if (node->right)
                delete_node(node->right);

        if (node)
                xfree(node);
}

void
free_tree(struct bintree *tree)
{
        delete_node(tree->rnode);

        tree->count = -1;
        xfree(tree);
}

struct node *
new_node(int item)
{
        struct node *node = xcalloc(1, sizeof(*node));

        node->item = item;
        node->left = node->right = NULL;
        node->parent = NULL;

        return node;
}

int 
node_lookup(struct node *node, int item)
{
        if (!node) {
                return 0;
        } else {
                if (item < node->item)
                        return node_lookup(node->left, item);
                else if (item > node->item)
                        return node_lookup(node->right, item);
                else
                        return 1;
        }
}

void
node_insert_iter(struct bintree *tree, int item)
{
        struct node *current = tree->rnode;

        if (current == NULL) {
                current = new_node(item);
                tree->rnode = current;
        } else {
                struct node *iter = current;

                while (iter != NULL) {

                        if (item <= iter->item) {
                                if (iter->left == NULL) {

                                        iter->left = new_node(item);
                                        iter->left->parent = iter;

                                        iter = NULL;
                                } else {
                                        iter = iter->left;
                                }
                        }  else {
                                if (iter->right == NULL) {

                                        iter->right = new_node(item);
                                        iter->right->parent = iter;

                                        iter = NULL;
                                } else {
                                        iter = iter->right;
                                }
                        }
                }
        }

        tree->count++;
}

void
node_insert(struct bintree *tree, int item)
{
        struct node *current = tree->rnode;

        if (current == NULL) {
                current = new_node(item);
        } else {
                if (item <= current->item) {
                        if (current->left == NULL) {
                                current->left = new_node(item);
                                current->left->parent = current;
                        } else {
                                tree->rnode = current->left;
                                node_insert(tree, item);
                        }
                }  else {
                        if (current->right == NULL) {
                                current->right = new_node(item);
                                current->right->parent = current;
                        } else {
                                tree->rnode = current->right;
                                node_insert(tree, item);
                        }
                }

        }
        /* restore root node */
        tree->rnode = current;

        tree->count++;
}

static
void __node_remove(struct node *node)
{
        node->left = node->right = NULL;
        node->parent = NULL;
        xfree(node);
}

/**
 * @brief removes a node from the tree which has only one or no children
 * @param node the node in question
 */
static void
__node_remove_single(struct node *rnode, struct node *node)
{
        struct node *child = NULL;

        /* determine if the node has childs */
        if (node->left) {
                child = node->left;
        }

        if (node->right) {
                child = node->right;
        }

        if (rnode == node) {
                node = child;
        } else {
                if (NODE_ON_LEFT(node))
                        node->parent->left = child;
                else
                        node->parent->right = child;

        }

        /* if the node has indeed a child reset its parent */
        if (child) {
                child->parent = node->parent;
        }

        __node_remove(node);
}

/**
 * @brief removes a node from the tree which has two children
 * @param node the node in question
 */
static void
__node_remove_with_children(struct node *rnode, struct node *node)
{
        struct node *max_left = node_get_max(node->left);

        if (max_left == node->left) {
                /*
                 * the largest node is immediately to the left
                 * of the node to remove. 
                 *
                 * bypass the node and transfer its right child
                 */
                max_left->right = node->right;
                max_left->right->parent = max_left;
        } else {
                /* largest node is somewhere deeper in the tree */
                max_left->parent->right = max_left->left;
                
                if (max_left->left != NULL)
                        max_left->left->parent = max_left->parent;

                /* replace the node with max_left */
                max_left->left = node->left;
                max_left->right = node->right;

                max_left->left->parent = max_left;
                max_left->right->parent = max_left;
        }

        if (node == rnode) {
                node = max_left;
        } else {
                if (NODE_ON_LEFT(node))
                        node->parent->left = max_left;
                else
                        node->parent->right = max_left;
        }

        max_left->parent = node->parent;

        __node_remove(node);
}

void
node_remove(struct node *rnode, struct node *node)
{
        if (node->left == NULL || node->right == NULL) {
                dprintf("node has only a child or no children\n");
                __node_remove_single(rnode, node);
        } else {
                dprintf("node has two children\n");
                __node_remove_with_children(rnode, node);
        }

}

int
tree_node_remove(struct bintree *tree, struct node *node)
{
        /* is there really there? */
        if (!node_lookup(node, node->item))
                return 0;

        node_remove(tree->rnode, node);

        tree->count--;
        return 1;
}

int
node_has_parent(struct node *node)
{
        if (node->parent) {
                return 1;
        }
        return 0;
}

/**
 * @brief print a node's item
 * @param a pointer to the node in question
 */
static void
__node_print(struct node *node)
{

        eprintf("Node \t%d\t (%p),\t", node->item, node);
        eprintf("p %p, left: %p, right: %p\n",
                node->parent, node->left, node->right);
}

static void
__node_print_paths(int *path, int len)
{
        int i;
        for (i = 0; i < len; i++) {
                eprintf("%d ", path[i]);
        }
        eprintf("\n");
}

void
node_print(struct node *node)
{
        if (node == NULL)
                return;

        if (node->left)
                node_print(node->left);

        __node_print(node);

        if (node->right)
                node_print(node->right);
}

void
node_print_post(struct node *node)
{
        if (node == NULL)
                return;

        if (node->left)
                node_print(node->left);
        if (node->right)
                node_print(node->right);

        __node_print(node);
}

void
node_print_pre(struct node *node)
{
        if (node == NULL)
                return;

        __node_print(node);

        if (node->left)
                node_print(node->left);
        if (node->right)
                node_print(node->right);
}

static void
node_print_paths(struct node *node, int *path, int path_len)
{
        if (node == NULL)
                return;

        assert(path_len != MAX_PATHS);

        /* append the node the path array */
        path[path_len++] = node->item;

        if (node->left == NULL && node->right == NULL) {
                __node_print_paths(path, path_len);
        } else {
                if (node->left)
                        node_print_paths(node->left, path, path_len);
                if (node->right)
                        node_print_paths(node->right, path, path_len);
        }
}

int
node_get_depth(struct node *node)
{
        int ld, rd;

        if (node == NULL)
                return 0;

        ld = node_get_depth(node->left);
        rd = node_get_depth(node->right);

        if (ld > rd) {
                return ld + 1;
        } else {
                return rd + 1;
        }
}

struct node *
node_get_max(struct node *node)
{
        struct node *iter = node;

        while (iter->right != NULL) {
                iter = iter->right;
        }

        return iter;
}

struct node *
node_get_min(struct node *node)
{
        struct node *iter = node;

        while (iter->left != NULL) {
                iter = iter->left;
        }

        return iter;
}

void
tree_print(struct bintree *tree)
{
        node_print(tree->rnode);
}

void
tree_print_path(struct bintree *tree)
{
        int *path = NULL;

        path = xcalloc(MAX_PATHS, sizeof(int));

        node_print_paths(tree->rnode, path, 0);

        xfree(path);
}

int 
tree_lookup_node(struct bintree *tree, int item)
{
        return node_lookup(tree->rnode, item);
}

int
tree_get_depth(struct bintree *tree)
{
        return node_get_depth(tree->rnode);
}


struct node *
tree_get_max(struct bintree *tree)
{
        struct node *node = tree->rnode;
        return node_get_max(node);
}

struct node *
tree_get_min(struct bintree *tree)
{
        struct node *node = tree->rnode;
        return node_get_min(node);
}

struct node *
tree_get_2nd_max(struct bintree *tree)
{
        struct node *max = node_get_max(tree->rnode);

        if (max->left != NULL) {
                /* walk on the right side to find the max */
                max = node_get_max(max->left);
        } else {
                /* go above one node */
                max = max->parent;
        }
        return max;
}

struct node *
tree_get_3nd_max(struct bintree *tree)
{
        struct node *nd_max = tree_get_2nd_max(tree);

        if (nd_max->left != NULL) {
                /* 
                 * same as finding the 2-nd max, go on the right 
                 * for the left node 
                 */
                nd_max = node_get_max(nd_max->left); 
        } else {

                /* 
                 * have to determine if the parent is the same node
                 * as the maximum node. In this case go to the parent
                 * of the parents' maximum node
                 */
                if (nd_max->parent == node_get_max(tree->rnode)) {
                        nd_max = nd_max->parent->parent;
                } else {
                        nd_max = nd_max->parent;
                }
        }
        return nd_max;
}


long int 
tree_count(struct bintree *tree)
{
        long int a = 1;
        struct node *root, *node;

        root = node = tree->rnode;

        if (node->left) {
                tree->rnode = node->left;
                a += tree_count(tree);
        }

        if (node->right) {
                tree->rnode = node->right;
                a += tree_count(tree);
        }

        /* restore root node */
        tree->rnode = root;
        return a;
}

/**
 * @brief recursively check if a tree is a BST
 * @param node 
 * @param min
 * @param max
 */
static int
__tree_is_bst(struct node *node, int min, int max)
{
        int rr, rl;


        if (node == NULL) {
                return 1;
        }

        if (node->item < min || node->item > max) {
                return 0;
        }

        rl = __tree_is_bst(node->left, min, node->item);
        rr = __tree_is_bst(node->right, node->item + 1, max);

        return rl && rr;
}

int
tree_is_bst(struct bintree *tree)
{
        int r;
        struct node *node = tree->rnode;

        r = __tree_is_bst(node, INT32_MIN, INT32_MAX);

        if (r) {
                return 1;
        }

        return 0;
}

static int 
__tree_eq(struct node *n1, struct node *n2)
{
        if (n1 == NULL || n2 == NULL) {
                return 1;
        } else if (n1 != NULL || n2 != NULL) {

                int l = __tree_eq(n1->left, n2->left);
                int r = __tree_eq(n1->right, n2->right);

                return ((n1->item == n2->item) && l && r);
        } else {
                return 0;
        }
}

int 
tree_eq(struct bintree *t1, struct bintree *t2)
{
        return __tree_eq(t1->rnode, t2->rnode);
}
