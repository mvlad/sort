#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>


#include "log.h"
#include "rand.h"
#include "xutil.h"
#include "timer.h"

int 
get_rand(long int len)
{
        unsigned seed;
        struct timespec ts;
        int c;

        get_current_ts(&ts);

        seed = (unsigned) ts.tv_nsec;
        srand(seed);

        while ((c = (rand() % len)) == 0)
                ;

        return c;
}

int 
irbit(unsigned long *iseed)
{
        /* The accumulated XOR's. */
        unsigned long newbit;   

        newbit =  ((*iseed >> 17) & 1)  /* get bit 18 */
                ^ ((*iseed >> 4) & 1)   /* xor with bit 5 */
                ^ ((*iseed >> 1) & 1)   /* xor with bit 2 */
                ^ (*iseed & 1);         /* xor with bit 1 */

        /* 
         * left shift the seed and put the result
         * of xors in its bit 1
         */
        *iseed = (*iseed << 1) | newbit;        

        return (int) newbit;
}

unsigned long int 
qrand(unsigned long seed)
{
        return (1664525L * seed) + 1013904223L;
}

int *
gen_array(long int len)
{
        int *res;
        long int i;

        struct timespec ts;
        unsigned seed;

        res = xcalloc(len + 1, sizeof(int));
        memset(res, 0, len + 1);

        int p = get_rand(20);
        dprintf("Populating array\n");
        for (i = 0; i < len; i++) {
                res[i] = i + p;
        }

        if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1) {
                dprintf("# clock_gettime()\n");
                exit(EXIT_FAILURE);
        }

        seed = (unsigned) ts.tv_nsec;
        srand(seed);

        dprintf("Shuffle array\n");
        for (i = len; i > 1; ) {
                int swp = (rand() % i--);

                int tmp = res[i];
                res[i] = res[swp];
                res[swp] = tmp;
        }


        return res;
}
