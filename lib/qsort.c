#include <stdio.h>

#include "log.h"
#include "xutil.h"
#include "qsort.h"

void 
__qsort(int *a, int n)
{
        int v;
        int l, r;

        if (n < 2) {
                return;
        }

        v = a[n / 2];

        /* pick one side at the beginning */
        l = 0;

        /* pick another side at the end */
        r = n - 1;

        while (r >= l) {

                if (a[l] < v) {
                        l++;
                        continue;
                }

                if (a[r] > v) {
                        r--;
                        continue;
                }

                /* swap the values */
                int tmp = a[l];

                a[l] = a[r];
                a[r] = tmp;

                l++; r--;
        }


        /* go on the left */
        __qsort(a + l, n - l);


        /* go on the right (note to adjust right) */
        __qsort(a, r + 1);

}

static void 
___qsort_s(int *a, int l, int r)
{
        int v, t;
        int i, j;

        if (l > r) {
                return;
        }

        v = a[r];

        i = l - 1;
        j = r;

        while (1) {
                while (a[++i] < v)
                        ; /* nothing */

                while (a[--j] > v)
                        ; /* nothing */

                if (i >= j)
                        break;

                t = a[i];
                a[i] = a[j];
                a[j] = t;
        }

        t = a[i];
        a[i] = a[r];
        a[r] = t;

        ___qsort_s(a, l, i - 1);
        ___qsort_s(a, i + 1, r);
}

void 
__qsort_s(int *a, int n)
{
        ___qsort_s(a, 0, n - 1);
}
