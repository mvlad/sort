#include "bsearch.h"

__inline int
__bsearch(int *a, int key, int n)
{
        int id;
        int min, max;

        min = 0;
        max = n - 1;

        while (max >= min) {
                id = MID_POINT(min, max);

                if (a[id] < key) {
                        min = id + 1;
                } else if (a[id] > key) {
                        max = id - 1;
                } else {
                        return id;
                }
        }
        return KEY_NOT_FOUND;
}

int 
__intersection(int *a, int *b, int m, int n, int *c)
{
        int i, j, x;

        x = 0;
        for (i = 0, j = 0; i < m && j < n;) {

                if (a[i] < b[j]) {
                        i++;    /* move forward */
                } else if (a[i] > b[j]) {
                        j++;    /* move forward */
                } else {        
                        /* equal, then save it */
                        c[x++] = b[j];

                        /* advance */
                        i++; j++;
                }
        }
        return x;
}

int 
__union(int *a, int *b, int m, int n, int *c)
{
        int i, j;
        int x;

        x = 0;
        for (i = 0, j = 0; i < m && j < n;) {

                if (a[i] < b[j]) {
                        /* take from the lower one */
                        c[x++] = a[i++];
                } else if (a[i] > b[j]) {
                        /* take from the bigger one */
                        c[x++] = b[j++];
                } else {
                        /* doesn't matter just add */
                        c[x++] = b[j];

                        /* advance */
                        i++; j++;
                }
        }

        /* 
         * we might have still items to processes as 
         *      i < m && j < n
         */
        while (i < m) {
                c[x++] = a[i++];
        }

        while (j < n) {
                c[x++] = b[j++];
        }

        return x;
}

