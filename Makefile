V := @

ifeq ($(V),1)
override V = 
endif

ifeq ($(V),0)
override V = @
endif

CC = gcc -MD -pipe
OPT_COMP = -O3
DBG = -g

FORTIFY  := -D_FORTIFY_SOURCE

INCLUDE = -Iinclude/
CFLAGS += -Wall -Wextra -Werror -Wstrict-prototypes -Wmissing-prototypes $(OPT_COMP)
CFLAGS += -ansi $(FORTIFY)=2 -D_POSIX_SOURCE -D_POSIX_C_SOURCE=200809L

# libs
LIBDIR := lib
# apps
BINS := bin
# where to dumb the objects
OBJDIR := obj

# debug for gdb or memgrind leaks
ifeq ($(DEBUG), 1)
CFLAGS_DEBUG := $(DBG)
CFLAGS_NEW := $(filter-out $(OPT_COMP),$(CFLAGS))
CFLAGS := $(CFLAGS_NEW) $(CFLAGS_DEBUG)
endif

# + debug messages
ifeq ($(DEBUG), 2)
CFLAGS_DEBUG := $(DBG) -DENABLE_DEBUG
CFLAGS_NEW := $(filter-out $(OPT_COMP),$(CFLAGS))
CFLAGS := $(CFLAGS_NEW) $(CFLAGS_DEBUG)
endif

# sanity with AddressSanitizer
ifeq ($(SANITIZE), 1)
CFLAGS_SANITIZE := $(DBG) -fsanitize=address
CFLAGS_NEW := $(filter-out $(OPT),$(CFLAGS))
CFLAGS := $(CFLAGS_NEW) $(CFLAGS_SANITIZE)
endif

# activate timer
ifeq ($(TIMER), 1)
CFLAGS += -DENABLE_TIMER
endif

LIB_SRC := $(LIBDIR)/xutil.c $(LIBDIR)/rand.c $(LIBDIR)/qsort.c \
	$(LIBDIR)/bsearch.c $(LIBDIR)/heap.c $(LIBDIR)/bintree.c
LIB_OBJS := $(subst $(LIBDIR),$(OBJDIR),$(LIB_SRC))
LIB_OBJS := $(LIB_OBJS:.c=.o)

# apps

# test BST
TREE_SRC := $(BINS)/bst.c
TREE_OBJS := $(subst $(BINS),$(OBJDIR),$(TREE_SRC))
TREE_OBJS := $(TREE_OBJS:.c=.o)

# test HEAP
HEAP_SRC := $(BINS)/mheap.c
HEAP_OBJS := $(subst $(BINS),$(OBJDIR),$(HEAP_SRC))
HEAP_OBJS := $(HEAP_OBJS:.c=.o)

# test qsort
QSORT_SRC := $(BINS)/sort.c
QSORT_OBJS := $(subst $(BINS),$(OBJDIR),$(QSORT_SRC))
QSORT_OBJS := $(QSORT_OBJS:.c=.o)

# test union/intersection
SETS_SRC := $(BINS)/sets.c
SETS_OBJS := $(subst $(BINS),$(OBJDIR),$(SETS_SRC))
SETS_OBJS := $(SETS_OBJS:.c=.o)

MARKDOWN := README.md

$(OBJDIR)/%.o: $(LIBDIR)/%.c
	$(shell test ! -d $(OBJDIR) && mkdir $(OBJDIR))
	$(V)$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@
	@echo "CC [lib] $@"

$(OBJDIR)/%.o: $(BINS)/%.c
	$(V)$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@
	@echo "CC [app] $@"

TARGETS := bst mheap sets sort

all:  $(TARGETS)

bst: $(LIB_OBJS) $(TREE_OBJS)
	$(V)$(CC) $(CFLAGS) -o $@ $(TREE_OBJS) $(LIB_OBJS) $(LDFLAGS)
	@echo "CC $@"

mheap: $(LIB_OBJS) $(HEAP_OBJS)
	$(V)$(CC) $(CFLAGS) -o $@ $(HEAP_OBJS) $(LIB_OBJS) $(LDFLAGS)
	@echo "CC $@"

sort: $(LIB_OBJS) $(QSORT_OBJS)
	$(V)$(CC) $(CFLAGS) -o $@ $(QSORT_OBJS) $(LIB_OBJS) $(LDFLAGS)
	@echo "CC $@"

sets: $(LIB_OBJS) $(SETS_OBJS)
	$(V)$(CC) $(CFLAGS) -o $@ $(SETS_OBJS) $(LIB_OBJS) $(LDFLAGS)
	@echo "CC $@"

cscope.files:
	$(V)rm -rf cscope* TAGS
	$(V)find . -maxdepth 1 -name \*.[chS] > cscope.files

TAGS:	cscope.files
	$(V)ctags -R -L cscope.files

tags:	cscope.files
	$(V)ctags -R -L cscope.files

cscope: cscope.files
	$(V)cscope -b -q -k

help:
	@echo "Options:"
	@echo "\tV=1		-- turn on verbose building"
	@echo "\tDEBUG=[12]	-- build with debug symbols and/or debug messages"
	@echo "\tSANITZE=1	-- build with AddressSanitizer"
	@echo "\tTIMER=1		-- enable timings of programs"
	@echo "Targets:"
	@echo "\tbst		-- build only bst program"
	@echo "\tmheap		-- build only mheap program"
	@echo "\tarrays_sort	-- build only arrays program"
	@echo "\tsort		-- build only sort program"
	@echo "\tclean		-- clean"

clean:
	$(V)rm -rf *.o cscope.* $(TARGETS) $(OBJDIR)/*.o $(OBJDIR)/*.d\
		callgrind* gmon* core.*
